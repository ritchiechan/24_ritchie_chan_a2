﻿using System;

namespace _Ritchie_Assignment2
{
    public class Game
    {
        public int Roll = 0;
        public int[] Rolls = new int[21];

        public void roll (int pinsDown)
        {
            Rolls[Roll++] = pinsDown;
        }

        public int Score()
        {
            int score = 0;
            int cursor = 0;

            for (int frame = 0; frame < 10; frame++)
            {
                if(isStrike(cursor))
                {
                     score += 10 + Rolls[cursor + 1] + Rolls[cursor + 2];
                    cursor += 1;
                }

                else if (isSpare(cursor))
                {
                     score += 10 + Rolls[cursor + 2];
                    cursor += 2;
                }

                else
                {
                    score += Rolls[cursor] + Rolls[cursor + 1];
                    cursor += 2;
                }
            }


            return score;



        }

        public object roll()
        {
            throw new NotImplementedException();
        }

        public Boolean isSpare(int cursor)

        {
            return Rolls[cursor] + Rolls[cursor + 1] == 10;

        }
        public Boolean isStrike(int cursor)

        {
            return Rolls[cursor] == 10;

        }



    }




    class MainClass
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
    }
}
