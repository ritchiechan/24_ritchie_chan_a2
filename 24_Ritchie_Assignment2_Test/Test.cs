﻿using NUnit.Framework;
using System;
using _Ritchie_Assignment2;

namespace _Ritchie_Assignment2_Test
{
    [TestFixture()]
    public class Test
    {
        Game game;

        [SetUp]

        public void SetUpGame()
        {
            game = new Game();
        }

        public void roll(int times, int pinsDown)
        {
            for (int i = 0; i < times; i++)
            {
                game.roll(pinsDown);
                
            }
        }

        [Test()]
        public void GutterGame()
        {
            roll(20, 0);
            int score = game.Score();
            Assert.That(score, Is.EqualTo(0));


        }
        [Test()]
        public void AllOnesGame()
        {
            roll(20, 1);
            int score = game.Score();
            Assert.That(score, Is.EqualTo(20));


        }
        [Test()]
        public void OneSpareGame()
        {
            roll(1, 5);
            roll(1, 5);
            roll(18, 0);
            int score = game.Score();
            Assert.That(score, Is.EqualTo(10));


        }
        [Test()]
        public void OneStrikeGame()
        {
            roll(1, 10);
            roll(19, 0);
            int score = game.Score();
            Assert.That(score, Is.EqualTo(10));


        }
        [Test()]
        public void AllStrikeGame()
        {
            roll(12, 10);
            int score = game.Score();
            Assert.That(score, Is.EqualTo(300));


        }
        [Test()]
        public void AllTwosGame()
        {
            roll(20, 2);
            int score = game.Score();
            Assert.That(score, Is.EqualTo(40));


        }
        [Test()]
        public void AllThressGame()
        {
            roll(20, 3);
            int score = game.Score();
            Assert.That(score, Is.EqualTo(60));


        }
        [Test()]
        public void AllFoursGame()
        {
            roll(20, 4);
            int score = game.Score();
            Assert.That(score, Is.EqualTo(80));


        }

    }


}
